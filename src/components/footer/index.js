import React from 'react';
import styled from 'styled-components';

// import Gap from '../atom/gap';

const FooterContainer = styled.div`
  background: ${(props)=> props.theme.navfooterbarColor};
  width: 100%;
  padding: 0.7rem 0;
  position: relative;
  top: 1rem;

  @media screen and (max-width: 992px){
    
  }
  @media screen and (max-width: 576px){
    width: 100%;
  }
`;
   
const FooterBox = styled.div`
  text-align: center;
  color: ${(props)=> props.theme.textColor};
`
;

function Footer() {
    return (
      <FooterContainer>
        <FooterBox>
        <h2>
          LOGO MICMI
        </h2>
        <div>© 2021 by Kebun Malam</div>
        </FooterBox>
      </FooterContainer>
    )
  }
  
export default Footer;
  