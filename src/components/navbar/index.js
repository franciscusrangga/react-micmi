import React, {useState} from 'react';
import styled from 'styled-components';

import Gap from '../atom/gap';

import {Modal} from '../common/modal';
import ButtonModal from '../common/button-modal';
import Search from '../common/search';
import DarkToggler from '../common/dark-toggler';
import { FaSearch, FaLongArrowAltRight, FaTimes } from "react-icons/fa";

const NavContainer = styled.div`
    background: ${(props)=> props.theme.navfooterbarColor};
    position: fixed;
    z-index: 10;
    width: 100%;

    nav{
        position: relative;
        display: flex;
        max-width: calc(100% - 8rem);
        margin: 0 auto;
        height: 70px;
        align-items: center;
        justify-content: space-between;
        @media screen and (max-width: 992px){
            max-width: 100%;
            padding: 0 20px;
        }
        @media screen and (max-width: 576px){
            padding: 0 10px;
        }
    }

`

const ButtonNavbar = styled.a`
    color: ${(props)=> props.theme.textColor};
    font-size: 1rem;
    font-weight: 500;
    padding: 7px 17px;
    border-radius: 5px;

    &:hover{
        text-decoration: underline;
    }

`

const SearchBar = styled.div`
    background: ${(props)=> props.theme.navfooterbarColor};;
    color: ${(props)=> props.theme.textColor};
    cursor: pointer;
    font-size: 18px;
    line-height: 70px;
    width: 50px;
    text-align: center;
    user-select: none;
`

const NavContent = styled.div`
    display: flex;
    align-items: center;
`

const NavLinks = styled.ul`
    margin-left: 30px;
    display: flex;
    @media screen and (max-width: 992px){
        display: block;
        position: fixed;
        background: ${(props)=> props.theme.navfooterbarColor};
        height: 100%;
        width: 100%;
        top: 70px;
        left: ${(props) => (props.clicked ? "0" : "-100%")};
        margin-left: 0;
        max-width: 350px;
        transition: all 0.3s ease;
    }
    @media screen and (max-width: 576px){
        max-width: none;
    }
`
const NavLinkTarget = styled.li`
    list-style: none;
    @media screen and (max-width: 992px){
        margin: 15px 20px;
    }
`
const ButtonNavTitle = styled.a`
    color: ${(props)=> props.theme.textColor};
    font-size: 1.5rem;
    font-weight: 600;
    @media screen and (max-width: 992px){
        line-height: 40px;
        font-size: 1.7rem;
        display: block;
        padding: 8px 18px;
    }
`;

const SearchBox = styled.form`
    display: flex;
    position: absolute;
    height: 100%;
    width: 100%;
    transition: all 0.3s ease;
`;

const SearchBarInput = styled.input`
    width: 100%;
    height: 100%;
    border: none;
    outline: none;
    font-size: 18px;
    color: ${(props)=> props.theme.textColor};
    background: ${(props)=> props.theme.navfooterbarColor};
    &::placeholder{
        color: ${(props)=> props.theme.textColor};
    }
`;

const RightBars = styled.div`
    display: flex;
    align-items: center;
`;

const GoBack = styled.button`
    position: absolute;
    right: 20px;
    top: 50%;
    transform: translateY(-50%);
    line-height: 60px;
    width: 50px;
    background: ${(props)=> props.theme.navfooterbarColor};
    border: none;
    color:  ${(props)=> props.theme.textColor};
    cursor: pointer;
    font-size: 20px;
    user-select: none;
`;

const MobileNavButton = styled.button`
    border: none;
    background: white;
    width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    cursor: pointer;
    display: none;
    position: relative;
    &::before, 
    &::after{
        content: "";
        background: black;
        height: 2px;
        width: 1rem;
        position: absolute;
        transition: all 0.3s ease;
    }
    &::before{
        top: ${(props) => (props.clicked ? "1.5" : "1rem")};
        transform:${(props) => (props.clicked ? "rotate(135deg)" : "rotate(0)")};
    }
    &::after{
        top: ${(props) => (props.clicked ? "1.2" : "1.5rem")};
        transform:${(props) => (props.clicked ? "rotate(-135deg)" : "rotate(0)")};
    }
    @media screen and (max-width: 992px){
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;

function NavbarContainer() {
    // const [theme, themeToggler, mountedComponent] = useDarkMode();
    const [clickSearchBar, setSearchBar] = useState(false);
    const handleSearchBarClick = () => setSearchBar(!clickSearchBar);

    const [clickMobile, setClick] = useState(false);
    const handleMobileClick = () => setClick(!clickMobile);

    return (
        <NavContainer>
            <nav>
            <MobileNavButton clicked={clickMobile} onClick={() => handleMobileClick()} ></MobileNavButton>
                <NavContent>
                    <div className="logo"></div> <ButtonNavTitle href="test">LOGOMICMI</ButtonNavTitle>
                    <NavLinks clicked={clickMobile}>
                        <NavLinkTarget> <ButtonNavbar href="home">Home</ButtonNavbar> </NavLinkTarget>
                        <NavLinkTarget> <ButtonNavbar href="explore">Explore</ButtonNavbar> </NavLinkTarget>
                        <NavLinkTarget> <ButtonNavbar href="about">About</ButtonNavbar> </NavLinkTarget>
                        <NavLinkTarget> <ButtonNavbar href="feedback">Feedback</ButtonNavbar> </NavLinkTarget>
                    </NavLinks>
                </NavContent>
                <RightBars>
                    <SearchBar onClick={() => handleSearchBarClick()} >
                        <FaSearch />
                    </SearchBar>
                    {/* <DarkToggler  /> */}
                </RightBars>
                {clickSearchBar && 
                    <SearchBox action="#">
                        <SearchBar onClick={() => handleSearchBarClick()} >
                            <FaTimes />
                        </SearchBar>
                        <SearchBarInput type="text" placeholder="Search for something..." />
                        <GoBack > <FaLongArrowAltRight/> </GoBack>
                    </SearchBox>
                }
                
            </nav>
        </NavContainer>
    )
}

export default NavbarContainer;

