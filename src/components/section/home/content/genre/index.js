import React, {Fragment} from 'react';
import styled from 'styled-components';
import ButtonTitle from '../../../../common/button-title';
import ContentGenre from '../../../../genreindex';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    background: ${(props)=>props.theme.baseColorwhitedarkgray};
    // @media screen and (max-width: 992px){
        
    // }
    @media screen and (max-width: 576px){
        flex-direction: column;
    }
`;
const DescGenre = styled.div`
    color: ${(props)=>props.theme.textColor};
`;

const Middle = styled.div`
    padding-top: 2rem;
    width: 90%;
    @media screen and (max-width: 576px){
        padding-left: 1.5rem;
    }

    .text{
        margin-left: 30px;
    }
`;

function GenreContent() {
    return (
        <Fragment>
            <Container>
                <Middle>
                    <ButtonTitle title="Genre" href="testgenre" />
                    <DescGenre class="text"> Pick Genre You Want </DescGenre>
                    <ContentGenre />
                </Middle>
            </Container>
        </Fragment>
    )
}


export default GenreContent;
