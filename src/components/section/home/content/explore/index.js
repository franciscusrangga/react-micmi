import React, {Fragment} from 'react';
import styled from 'styled-components';
import MultiSlider from '../../../../card/multislider';
import {MultiSliderData} from '../../../../card/multislider/multisliderdata.js';
import ButtonTitle from '../../../../common/button-title';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    // @media screen and (max-width: 992px){
    //     // padding: 0 1rem;
    // }
    @media screen and (max-width: 576px){
        flex-direction: column;
    }
`;

const Middle = styled.div`
    padding-top: 2rem;
    width: 90%;
    @media screen and (max-width: 576px){
        padding-left: 1.5rem;
    }
`;

function ExploreContent() {
    return (
        <Fragment>
            <Container>
                <Middle>
                    <ButtonTitle title="Explore" href="test" />
                    <MultiSlider slides={MultiSliderData} />
                </Middle>
            </Container>
        </Fragment>
        
    )
}

export default ExploreContent;
