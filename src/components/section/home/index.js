import React, {Fragment} from 'react';
import styled from 'styled-components';

import Slider from '../../card/slider';
import {SliderData} from '../../card/slider/sliderdata.js';
import Favorite from '../../card/favorite';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    background: ${(props)=> props.theme.baseColorwhitedarkgray};
    @media screen and (max-width: 992px){
        // flex-direction: column;
        flex-direction: column;
    }
    @media screen and (max-width: 576px){
        flex-direction: column;
    }
`;

const Middle = styled.div`
    // padding-left: 6rem;
    @media screen and (max-width: 992px){
        padding-left: 0;
    }
    @media screen and (max-width: 576px){
        padding-left: 0;
    }
`;
const Right = styled.div`
    // padding-right: 6rem;
    @media screen and (max-width: 992px){
        padding-right: 0;
    }
    @media screen and (max-width: 576px){
        padding-left: 0;
    }
`;

function HomeSection() {
    return (
        <Fragment>
            <Container>
                <Middle>
                    <Slider slides={SliderData} />
                </Middle>
                <Right>
                    <Favorite />
                </Right>
            </Container>
        </Fragment>
    )
}

export default HomeSection;
