import React from 'react';
import styled from 'styled-components';
import Photo1 from '../../../assets/thumb/photo-1.jpg';
import ButtonGenre from '../../common/button-genre';
import Gap from '../../atom/gap';
import ButtonMuse from '../../common/button-muse';


const Container = styled.div`
    padding: 0.1rem 5rem;
    background-color: ${(props) => props.theme.baseColor};
    display: flex;
    justify-content: center;
    flex-direction: row;
    &::before, 
    &::after{
        transition: all 0.3s ease;

    @media screen and (max-width: 576px){     
        flex-direction: column;
        justify-content: center;
        padding: 0.1rem 1rem;
    }  
            .hidden{
                @media screen and (max-width: 576px){
                    display: none;
                }
            }
`;
const TitleMovie = styled.div`
    font-size: 30px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between; 
    color:  ${(props) => props.theme.textColor};
    // background-color:  ${(props) => props.theme.baseColorwhitedarkblue};
    // padding: 1rem 5rem;
    // padding-top: 1rem;
    // margin: -0.1rem -5rem;
    
        .ratings{
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 10rem;

                .rating{
                    font-size: 15px;
                }
        }
            @media screen and (max-width: 576px){
                flex-direction: column;
                align-items: center;
            }
    // @media screen and (min-width: 1024px){
            
    //      margin: -0.1rem -5rem;
    
    // }
        

`;
const CoverMovie = styled.image`
    display: flex;
    justify-content: space-evenly   ;
    background-color: ${(props) => props.theme.baseColor};
    // padding-top: 1rem;
    // margin: -0.1rem -5rem;
    
        .cover{
            width: 286px;
            height: 423px;
            
        }

            .trailer {
                display: flex;
                frameborder = "0"
                allow = "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
                height: 423px;

                @media screen and (max-width: 992px){
                        width: 530px;
                        height: 400px;
                }

                @media screen and (max-width: 576px){
                        width: 286px;
                        height: 200px;
                }
            }

        @media screen and (max-width: 992px){
            flex-direction: column;
            align-items: center;


        }
        // @media screen and (min-width: 1024px){
            
        //     margin: -0.1rem -5rem;

        // }
        
            
        
`;
const ButtonMovie = styled.div`
    display: flex;
    justify-content: space-between;
    background-color: ${(props) => props.theme.baseColor};      
    // padding: 2rem 5rem;
    // margin: -0.1rem -5rem;
    padding-bottom: 2rem;
    
        .btn-muse {
            width: 100px;
        }    
                .button-genres{
                    display: flex;
                    gap: 0.5rem;
                    width: 30rem;
                    flex-wrap: wrap;

                    @media screen and (max-width: 576px){
                        width: 100%;
                        justify-content: center;
                    }
                }

    @media screen and (max-width: 576px){
        flex-direction: column-reverse;
        align-items: center;
    }
    // @media screen and (min-width: 1024px){
            
    //     margin: -0.1rem -5rem;

    // }
`;
const InfoMovie = styled.div`
    .movie-desc{
        border: 2px solid lightgray;
        border-radius: 20px;
        width: 30%;
    }
        .font-bold{
            font-weight: bold;
        }
`;
const TextMovie = styled.div`
    color:  ${(props) => props.theme.textColor}; 
    background-color: ${(props) => props.theme.baseColorwhitedarkgray};
    padding: 2rem 5rem;
    margin: -0.1rem -5rem;
    
    @media screen and (min-width: 1024px){
            
            margin: -0.1rem -5rem;
    
        }
`;
const Middle = styled.div`
       
    
`;

function MovieDetailSection({}) {
    return (
        <Container>
            <Middle>
            <Gap class height={40}  />
            <TitleMovie>                
                <h3>Takt Op. Destiny</h3>
                <div class="ratings">
                    <p class="rating hidden">Rating</p>
                    <p class="rating">✰ 9.0/10 </p> 
                </div>
            </TitleMovie>
            <Gap class height={40}  />
            <CoverMovie>
                <img class="cover" src={Photo1} alt="mushoku" />
                <Gap width={170} height={10} />
                <iframe class = "trailer" width = "739"
                src = "https://www.youtube.com/embed/3Zjbuem7ceg" 
                title = "YouTube video player" >
                </iframe>
            </CoverMovie>
            <Gap class height={40}  />
            <ButtonMovie>
                <div class="button-genres">
                    <ButtonGenre href='action' title='Action' />
                    <ButtonGenre href='fantasy' title='Fantasy' />
                    <ButtonGenre href='action' title='Action' />
                    <ButtonGenre href='action' title='Action' />
                    <ButtonGenre href='action' title='Action' />
                    <ButtonGenre href='action' title='Action' />
                </div>
                <Gap height={40} />
                <div>
                    <ButtonMuse title='Watch In' width={18} height={4} href="halo"  />
                </div>
            </ButtonMovie>
           
            <TextMovie>
                <p>
                    Berlatar pada tahun 2047, suatu hari terdapat Meteorit Hitam yang jatuh dan menimpat bumi. Setelah kedatangan meteorit ini, banyak manusia, hewan, dan juga tumbuhan yang mulai hancur satu per satu. Selain itu, ada juga monster yang sangat menyeramkan bernama D2.
                    <br/> <br/>
                    Menariknya, monster ini telah memberlakukan aturan bahwa seluruh manusia dilarang memainkan alat musik. Semua manusia tunduk atas perintahnya dan tidak ada yang berani memainkan musik ataupun melanggar aturan tersebut.
                    <br/> <br/>
                    Walau begitu, tidak semua manusia ingin menuruti kemauan sang monster. Beberapa gadis muda justru melawan D2 dengan musik yang mana mereka disebut dengan Musicart. Setiap Musicart dipimpim oleh seorang Konduktor. Kini, cerita mulai berpusat kepada Takt, seorang pria yang ingin memainkan music di New York. Untuk itu, ia mengajak seorang wanita bernama Unmei untuk menjadi Musicard dan Takt sebagai Konduktor.
                </p>
                <Gap height={10} />
                <InfoMovie>
                    <div>
                        <div class="movie-desc"></div>
                        <p><span class="font-bold">STUDIO</span> Madhouse, MAPPA</p>
                    </div>
                    <div>
                        <div class="movie-desc"></div>
                        <p><span class="font-bold">AIRED</span> Oct 6, 2021</p>
                    </div>
                    <div>
                        <div class="movie-desc"></div>
                    </div>
                </InfoMovie>
                <Gap height={30} />
            </TextMovie>
            </Middle> 
        </Container>
    )
}


export default MovieDetailSection;
