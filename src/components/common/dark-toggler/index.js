import React, {useState, useEffect} from 'react';

import styled from "styled-components";
import { CgSun } from "react-icons/cg";
import { HiMoon } from "react-icons/hi";

const Toggle = styled.button`
    position: fixed;
    top: 10px;
    right:32px;
    cursor: pointer;
    height: 40px;
    width: 40px;
    border-radius: 10px;
    border: 2px solid blue;
    background-color: transparent;
    color: blue;
    z-index: 10;
    &:focus {
        outline: none;
    }
    transition: all .5s ease;
    &:hover{
        background-color: ${({ theme }) => theme.secondaryBackground};
        transition: none;
    }
    @media screen and (max-width: 992px){
        top: 80px;
        right: 15px;
    }
`;


function DarkToggler({theme,  toggleTheme }) {
    // const icon = props.theme === "dark" ? <HiMoon size={30} /> : <CgSun size={30} />;

    // const updateThemeDark = () => {
    //     localStorage.setItem('theme', 'dark');
    //     // props.setTheme("dark");
    // }
    // const updateThemeLight = () => {
    //     localStorage.setItem('theme', 'light');
    // }

    return (
        <div>
            <Toggle onClick={toggleTheme }>
                {/* {icon} */}
                switch Theme
            </Toggle>
            {/* <Toggle onClick={() => updateThemeLight()}>
                test light
            </Toggle> */}
        </div>
    )
}

export default DarkToggler;