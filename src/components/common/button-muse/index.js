import React, {Fragment} from 'react';
import styled from 'styled-components';

import logoYoutube from '../../../assets/logo/youtube.jpg';
import logoMuse from '../../../assets/logo/muse.jpg';
import {FaPlay} from 'react-icons/fa';
import Gap from '../../atom/gap';

const ButtonMuseContainer = styled.a`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 0.2rem 0.2rem;
    border-radius: 20px;
    color: var(--color-dark);
    cursor: pointer;
    background: #FB000F;
    width: ${(props) => props.width || 10}rem;
    height: ${(props) => props.height || 2}rem;

    img{
        width: 3rem;
        height: 2rem;
        border-radius: 20px;
    }
    span{
        color: white;
    }
`;
const Play = styled.i`
    margin-top: 5px;
    color: white;
`;

function ButtonMuse({title, width, height, href, small}) {
    return (
        <Fragment>
            {!small ? <ButtonMuseContainer width={width} href={href} height={height}>
                <Gap width={10} height={20}/>
                <Play><FaPlay /></Play>
                <Gap width={10} height={20}/>
                <span>{title}</span>
                <Gap width={10} height={20}/>
                <img src={logoYoutube} alt="" />
                <Gap width={10} height={20}/>
                <img src={logoMuse} alt="" />
                <Gap width={10} height={20}/>
            </ButtonMuseContainer> : null}
            {small ? <ButtonMuseContainer width={width} href={href}>
                <span>{title}</span>
            </ButtonMuseContainer> : null}
         </Fragment>
    )
}

export default ButtonMuse;
