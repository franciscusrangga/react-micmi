import React, {Fragment} from 'react';
import styled from 'styled-components';
import { FaList } from "react-icons/fa";
import Gap from '../../atom/gap';

const ButtonModalContainer = styled.button`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border: 1px solid var(--color-dark);
    padding: 0 0.5rem;
    border-radius: 0.5rem;
    color: var(--color-dark);
    cursor: pointer;

    img{
        width: 2rem;
    }
`;

function ButtonModal({title, openModal}) {
    return (
        <Fragment>
            <ButtonModalContainer onClick={openModal}>
                <FaList />
                <Gap width={5} height={0} />
                <span>{title}</span>
            </ButtonModalContainer>
         </Fragment>
    )
}

export default ButtonModal;
