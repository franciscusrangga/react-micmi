import React from 'react';
import styled from 'styled-components';

import { FaSearch } from "react-icons/fa";

const SearchBar = styled.div`
    background: var(--color-white);
    border-radius: var(--border-radius);
    border: 1px solid var(--color-black);
    padding: 0 0.7rem;
    display: -ms-flexbox;
    justify-content: center;
    align-items: center;

    i{
        color: black;
    }
    
    @media screen and (max-width: 992px){
        display: -ms-flexbox;
    }

    @media screen and (max-width: 576px){
        width: 100%;
    }

`;
const SearchInput = styled.input`
    width: 25rem;
    padding: 1rem 0;
    border: none;
    background: transparent;
    font-size: 1rem;
    color: var(--color-black);

    ::placeholder {
        color: var(--color-black);
    }

    @media screen and (max-width: 992px){
        width: 15rem;
    }

    @media screen and (max-width: 576px){
        width: 100%;
    }
`;


function Search() {
    return (
        <SearchBar>
            <SearchInput placeholder="Search for something"/>
            <button className="btn">
                <FaSearch />
            </button>
         </SearchBar>
    )
}

export default Search;
