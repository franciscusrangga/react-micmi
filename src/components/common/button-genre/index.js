import React, {Fragment} from 'react';
import styled from 'styled-components';

const ButtonGenreContainer = styled.a`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 0.2rem 0.8rem;
    border-radius: 20px;
    color: var(--color-dark);
    cursor: pointer;
    background: #002c66;
    font-size: 15px;
    color: white;
    width: ${(props) => props.width || 7}rem;

        // .decorate{
        // display: flex;
        // flex-direction: row;
        // }
`;



function ButtonGenre({href, title}) {
    return (
    <Fragment>
        <ButtonGenreContainer href={href}>
            <p> {title} </p>
        </ButtonGenreContainer>
    </Fragment>
    )
}

export default ButtonGenre;