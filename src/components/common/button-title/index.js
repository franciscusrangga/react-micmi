import React, {Fragment} from 'react';
import styled from 'styled-components';

import {FaArrowRight} from 'react-icons/fa';
import Gap from '../../atom/gap';

const ButtonTitleContainer = styled.a`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    padding: 0.2rem 0.2rem;
    border-radius: 20px;
    color: ${(props)=> props.theme.textColor};
    cursor: pointer;

    .decorate{
        height: 3rem;
        border: 5px solid ${(props)=> props.theme.textColor};
        border-radius: 20px;
    }
`;
const CrossRight = styled.i`
    margin-top: 12px;
    ${(props)=> props.theme.textColor};
    font-size: 2rem;
`;

function ButtonTitle({href, title}) {
    return (
        <Fragment>
            <ButtonTitleContainer href={href}>
                <div className="decorate"></div>
                <Gap width={10} height={25} />
                <h3>{title}</h3>
                <Gap width={10} height={25} />
                <CrossRight><FaArrowRight /></CrossRight>
            </ButtonTitleContainer>
         </Fragment>
    )
}

export default ButtonTitle;
