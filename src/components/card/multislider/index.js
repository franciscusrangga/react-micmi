import React from 'react';
import {MultiSliderData} from './multisliderdata.js';
import {FaArrowRight, FaArrowLeft, FaRegStar} from 'react-icons/fa';
import './multislider.css';

import styled from 'styled-components';
import ButtonMuse from '../../common/button-muse/index.js';

const SliderContainer = styled.div`
    width: 100%;
    padding: 2rem;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;

    .faCircleBox{
        display: flex;
        position: absolute;
        font-size: 3rem;
        z-index: 8;
        user-select: none;
        cursor: pointer;
        background: linear-gradient(#6B6B6B,  #AFAFAF);
        opacity: 90%;
        border: 2px solid ${(props)=> props.theme.baseColorwhitedarkgray};
        padding: 1rem 0.3rem;
        color: ${(props)=> props.theme.baseColorwhitedarkgray};

        @media screen and (max-width: 576px){
            display: none;
        }

    }

`;

const SliderCardSingle  = styled.div`
    width: 11rem;
    height: 20rem;
    background: ${(props)=> props.theme.baseColorwhitedarkblue};
    border-radius: 10px;
    display: inline-block;
    margin-left: 5px;
    margin-right: 5px;
    box-shadow: 2px 2px 2px 2px rgb(0 0 0 / 12%);
`;

const PhotoThumb = styled.img `
    width: 100%;
    height: 220px;
    background-color: rgb(240 240 240 / 80%);
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    &:hover{
        opacity: 70%;
    }
`;

const CardContainer = styled.div`
    width: 11rem;
    height: 6rem;
    padding: 0.5rem;
    display:flex;
    flex-direction: column;
    justify-content: space-between;
    border-radius: 10px;
    text-align: center;
`;

const TitleCardSlider = styled.a`
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    &:hover{
        text-decoration: underline;
    }
`

const FaCircleRight  = styled.div`
    top: 20%;
    right: 0;
`;

const FaCircleLeft  = styled.div`
    top: 20%;
    left: 0;
`;

const CardRating = styled.div`
    color: ${(props)=> props.theme.textColor};
    display: flex;
    align-items: center;
    gap: 0.2rem;
    padding: 0.2rem;
    border-radius: 10px;
    background: ${(props)=> props.theme.baseColor};
    width: 4rem;

    position: relative;
    bottom: 8.5rem;
    left: 6.5rem;

    @media screen and (max-width: 576px){
        bottom: 70%;
    }

`;


function MultiSlider() {
    const slideLeft =() =>{
        var slider = document.getElementById("slider");
        slider.scrollLeft = slider.scrollLeft + 500;
    }

    const slideRight =() =>{
        var slider = document.getElementById("slider");
        slider.scrollLeft = slider.scrollLeft - 500;
    }

    return (
        <SliderContainer>
            <FaCircleRight className="faCircleBox" onClick={slideLeft} >
                <FaArrowRight />
            </FaCircleRight>
            <div id="slider">
                {MultiSliderData.map((slide, index) => {
                    return (
                        <SliderCardSingle>
                            <PhotoThumb src={slide.image} alt="anime photo" />
                            <CardContainer>
                                <TitleCardSlider href="test">{slide.title}</TitleCardSlider>
                                <ButtonMuse title="Watch" small="true" />
                            </CardContainer>  
                            <CardRating>
                                <FaRegStar /> <p>{slide.rating}</p>
                            </CardRating>
                        </SliderCardSingle>
                    )
                })}
            </div>
            <FaCircleLeft className="faCircleBox" onClick={slideRight}>
                <FaArrowLeft />
            </FaCircleLeft>          
        </SliderContainer>
    )
}

export default MultiSlider
