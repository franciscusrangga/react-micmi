import Photo1 from '../../../assets/thumb/photo-1.jpg';
import Photo2 from '../../../assets/thumb/photo-2.jpg';
import Photo3 from '../../../assets/thumb/photo-3.jpg';

export const MultiSliderData = [
    {
        image: Photo1,
        title: 'TAKT OP. DESTINY 1 1 1 1 1  1 1 1',
        rating: '10.00',
    },
    {
        image: Photo2,
        title: 'Assassins Creed Revelation',
        rating: '8.00',
    },
    {
        image: Photo3,
        title: 'MUSHOKU TENSEI 1',
        rating: '7.00',
    },
    {
        image: Photo1,
        title: 'TAKT OP. DESTINY 2',
        rating: '9.00',
    },
    {
        image: Photo2,
        title: 'SEKAI ASSASSINS 2',
        rating: '8.00',
    },
    {
        image: Photo3,
        title: 'MUSHOKU TENSEI 2',
        rating: '7.00',
    },
    {
        image: Photo1,
        title: 'TAKT OP. DESTINY 3',
        rating: '9.00',
    },
    {
        image: Photo2,
        title: 'SEKAI ASSASSINS 3',
        rating: '8.00',
    },
    {
        image: Photo3,
        title: 'MUSHOKU TENSEI 3',
        rating: '7.00',
    },
    {
        image: Photo1,
        title: 'TAKT OP. DESTINY 4',
        rating: '9.00',
    },
    {
        image: Photo2,
        title: 'SEKAI ASSASSINS 4',
        rating: '8.00',
    },
    {
        image: Photo3,
        title: 'MUSHOKU TENSEI 4',
        rating: '7.00',
    },
]