import React, {Fragment} from 'react';
import {favoriteData} from './favoritedata.js';

import Gap from '../../atom/gap';

import styled from 'styled-components';

import ButtonMuse from '../../common/button-muse';

const FavoriteContainer = styled.div`
    position: relative;
    padding: 2rem 1rem;
    @media screen and (max-width: 576px){
        display: flex;
        align-items: center;
        flex-direction: column;
    }
    p{
        color: ${(props)=> props.theme.textColor};
    }
`;

const FavoriteCard = styled.div`
    display: flex;
    background: #002c66;
    padding: 1rem;
    border-radius: 20px;
    align-items: center;
    width: 100%;
    color: white;
    @media screen and (max-width: 992px){
        justify-content: space-between;
    }
    @media screen and (max-width: 576px){
        justify-content: center;
        text-align: center;
        flex-wrap: wrap;
        width: 80%;
    }
`;

const PhotoCover = styled.img`
    width: 5rem;
    height: 8rem;
    @media screen and (max-width: 576px){
        width: 7rem;
        height: 10rem;

        position: relative;
        bottom: 70%;

    }
`;

const FavDes = styled.div`
    width: 10rem;
`;

const TitleFavorite = styled.a`
    color: white;
    font-weight: bold;
    font-size: 1.2rem;
    &:hover{
        text-decoration: underline;
    }
`

function Favorite() {
    return (
        <FavoriteContainer>
            <p>Favorites</p>
            {favoriteData.map((card, index) => {
                return (
                    <Fragment>
                        <FavoriteCard>
                            <h1>{card.rank}</h1>
                            <Gap width={20} height={10} />
                            <PhotoCover src={card.photo}></PhotoCover>
                            <Gap width={20} height={10} />
                            <FavDes>
                                <TitleFavorite href="test">{card.title}</TitleFavorite>
                                <Gap width={0} height={60} />
                                <ButtonMuse href={card.slug} />
                            </FavDes>
                        </FavoriteCard>
                        <Gap width={20} height={10} />
                    </Fragment>
                )
            })}
        </FavoriteContainer>
    )
}

export default Favorite
