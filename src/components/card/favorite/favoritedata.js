import Photo1 from '../../../assets/thumb/photo-1.jpg';
import Photo2 from '../../../assets/thumb/photo-2.jpg';
import Photo3 from '../../../assets/thumb/photo-3.jpg';

export const favoriteData = [
    {
        rank: 1,
        photo: Photo1,
        title: 'Takt Op. Destiny',
        slug: 'takt-op-destiny',
    },
    {
        rank: 2,
        photo: Photo2,
        title: 'Sekai Saikou no Ansastsu',
        slug: 'sekai-saikou-no',
    },
    {
        rank: 3,
        photo: Photo3,
        title: 'Mushoku Tensei',
        slug: 'mushoku-tensei',
    },
]