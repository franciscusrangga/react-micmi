import React, {useState} from 'react';
import {SliderData} from './sliderdata.js';
import {FaArrowRight, FaArrowLeft, FaRegStar} from 'react-icons/fa';
// import './slider.css';

import styled from 'styled-components';

const SliderContainer = styled.div`
    padding: 5rem 3rem;
`;
const SliderBox = styled.div`
    position: relative;
    height: 25rem;
    display: flex;
    justify-content: center;
    align-items: center;

    .faCircleBox{
        display: flex;
        position: absolute;
        font-size: 3rem;
        z-index: 8;
        user-select: none;
        cursor: pointer;
        background: linear-gradient(#6B6B6B,  #AFAFAF);
        opacity: 90%;
        border: 2px solid ${(props)=> props.theme.baseColorwhitedarkgray};
        padding: 1rem 0.3rem;
        color: ${(props)=> props.theme.baseColorwhitedarkgray};
    }

`;

const PhotoThumb = styled.img `
    width: 35rem;
    height: 25rem;
    object-fit: cover;
    &:hover{
        opacity: 60%;
    }

    @media screen and (max-width: 992px){
        // padding: 0 1rem;
    }
    @media screen and (max-width: 576px){
        max-width: 20rem;
    }
`;

const CardContainer = styled.div`
    position: absolute;
    top: 68%;
    background: ${(props)=> props.theme.baseColorwhitedarkgray};
    opacity: 90%;
    width: 35rem;
    height: 8rem;

    display: flex;
    flex-direction: row;
    justify-content: space-around;
    align-items: flex-end;

    a{
        position: relative;
        bottom: 3rem;
        color: ${(props)=> props.theme.textColor};
        &:hover{
            text-decoration: underline;
        }
    }

    @media screen and (max-width: 992px){
        // padding: 0 1rem;
    }
    @media screen and (max-width: 576px){
        gap: 0.2rem;
        flex-direction: column;
        align-items: center;
        max-width: 20rem;
        height: 8rem;

        a{
            position: relative;
            bottom: 5.5rem;
        }

    }

`;

const FaCircleRight  = styled.div`
    top: 20%;
    right: 0;
`;

const FaCircleLeft  = styled.div`
    top: 20%;
    left: 0;
`;

const PhotoCover = styled.img`
    width: 8rem;
    height: 12rem;
    &:hover{
        transform: scale(1.4)
    }
    @media screen and (max-width: 576px){
        width: 6rem;
        height: 10rem;

        position: relative;
        bottom: 70%;

    }
`;

const CardRating = styled.div`
    display: flex;
    align-items: center;
    gap: 0.2rem;
    border: 1px solid ${(props)=> props.theme.textColor};
    padding: 0.2rem;
    border-radius: 10px;
    color: ${(props)=> props.theme.textColor};

    position: relative;
    bottom: 1rem;

    @media screen and (max-width: 576px){
        bottom: 70%;
    }

`;


function Slider({slides}) {
    const [current, setCurrent] = useState(0);
    const length = slides.length;

    const nextSlide = () => {
        setCurrent(current === length - 1 ? 0 : current + 1);
    };

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 1 : current - 1);
    };

    if(!Array.isArray(slides) || slides.length <= 0) {
        return null;
    } 

    return (
        <SliderContainer>
            <SliderBox>
                <FaCircleRight className="faCircleBox" onClick={nextSlide}>
                    <FaArrowRight />
                </FaCircleRight>
                <FaCircleLeft className="faCircleBox" onClick={prevSlide}>
                    <FaArrowLeft />
                </FaCircleLeft>
                {SliderData.map((slide, index) => {
                    return  (
                        <div className={index === current ? 'slide active' : 'slide'} key={index}>
                            {index === current && (
                                <div>
                                    <PhotoThumb src={slide.image} alt="anime photo" />
                                    <CardContainer>
                                        <PhotoCover src={slide.image}></PhotoCover>
                                        <a href="page/moviedetail"><h2>{slide.title}</h2></a>
                                        <CardRating>
                                            <FaRegStar /> <p>{slide.rating}</p>
                                        </CardRating>
                                    </CardContainer>
                                </div>
                            )}
                        </div>
                    )
                })}
            </SliderBox>
        </SliderContainer>
    )
}

export default Slider
