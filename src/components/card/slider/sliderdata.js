import Photo1 from '../../../assets/thumb/photo-1.jpg';
import Photo2 from '../../../assets/thumb/photo-2.jpg';
import Photo3 from '../../../assets/thumb/photo-3.jpg';

export const SliderData = [
    {
        image: Photo1,
        title: 'TAKT OP. DESTINY',
        rating: '9.00',
    },
    {
        image: Photo2,
        title: 'SEKAI ASSASSINS',
        rating: '8.00',
    },
    {
        image: Photo3,
        title: 'MUSHOKU TENSEI',
        rating: '7.00',
    }
]