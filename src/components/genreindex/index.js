import React, {Fragment} from 'react';
import styled from 'styled-components';
// import Gap from '../atom/gap';
import { Genreindex } from './genredata';


const Container = styled.div`
    display: flex;
    flex-direction: row;
    gap: 1rem;
    justify-content: flex-start;
    padding: 0 2rem;
    padding-bottom: 4rem;
    @media screen and (max-width: 992px){
       padding-left: 0;
       padding-right: 0;
       justify-content: center;
       flex-wrap: wrap;
    }
    @media screen and (max-width: 576px){
        display: flex;
        justify-content: center;
    }
`;

const ButtonContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 0.2rem 0.8rem;
    border-radius: 20px;
    color: white;
    cursor: pointer;
    background: #002c66;
    font-size: 15px;
    margin-top: 20px;
    font-color: black;
    width: ${(props) => props.width || 7}rem;
    
    @media screen and (max-width: 992px){
        padding: 0.2rem 0.8rem;
        justify-content: center;
    }

    @media screen and (max-width: 576px){
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
    }
`;



function ContentGenre(){
    return(
        <Fragment>
            <Container>
                {Genreindex.map((data, genre) => {
                return (
                    <ButtonContainer href={data.href}>
                        <p>{data.title}</p> 
                    </ButtonContainer>
                )
                })}  
            </Container>
        </Fragment>
    )
}

export default ContentGenre;