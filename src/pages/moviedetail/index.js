import React, { Component, Fragment } from 'react';
import NavbarContainer from '../../components/navbar';
import MovieDetailSection from '../../components/section/moviedetail';
import Footer from '../../components/footer';

class MovieDetail extends Component {
    render() {
        return (
            <Fragment>
                <nav>
                    <NavbarContainer />
                </nav>
                <main style={{ top: "4rem" }}>
                    <MovieDetailSection />
                </main>
                <footer style={{ paddingTop: "5rem" }}>
                    <Footer />
                </footer>
            </Fragment>
        )
    }
}

export default MovieDetail;

