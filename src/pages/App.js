// import logo from './logo.svg';
import {Routes} from '../config';
import './App.css';

import DarkToggler from '../components/common/dark-toggler';

import { ThemeProvider } from 'styled-components';

//THEME CONFIG
import {GlobalStyles, LightTheme, DarkTheme } from '../theme';
import  {useDarkMode} from "../useTheme";

function App() {
  const [theme, themeToggler, mountedComponent] = useDarkMode();

  const themeMode = theme === 'light' ? LightTheme : DarkTheme;

  if(!mountedComponent) return <div/>
  return (
    <ThemeProvider theme={themeMode}>
      <GlobalStyles />
      <Routes /> 
      <DarkToggler theme={theme} toggleTheme={themeToggler} />
    </ThemeProvider>
  );
}

export default App;