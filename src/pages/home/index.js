import React, { Component, Fragment } from 'react';
import NavbarContainer from '../../components/navbar';
import HomeSection from '../../components/section/home';
import ExploreContent from '../../components/section/home/content/explore';
import GenreContent from '../../components/section/home/content/genre';
import Footer from '../../components/footer';

class Home extends Component {
    render() {
        return (
            <Fragment>
                <nav>
                    <NavbarContainer />
                </nav>
                <main>
                    <HomeSection />
                    <ExploreContent />
                    <GenreContent />
                </main>
                <footer style={{ paddingTop: "5rem" }}>
                    <Footer />
                </footer>
            </Fragment>
        )
    }
}

export default Home;
