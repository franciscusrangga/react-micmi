import {createGlobalStyle} from 'styled-components';

export const LightTheme = {
    baseColor: "#f7f7f8",
    baseColorwhitedarkgray: "white",
    baseColorwhitedarkblue: "white",
    baseColorbluedarkblue: "#2496ed",
    secondaryColor: "#2496ed",
    navfooterbarColor: "#fff",
    textColor: "black",
}
export const DarkTheme = {
    // baseColor: "#031f30",
    baseColor: "#031f30",
    baseColorwhitedarkgray: "#181818",
    baseColorwhitedarkblue: "#0f161e",
    baseColorbluedarkblue: "#0f161e",
    secondaryColor: "#0f1c24",
    navfooterbarColor: "#303030",
    textColor: "white",
}

export const themes = {
    light: LightTheme,
    dark: DarkTheme,
}

export const GlobalStyles = createGlobalStyle`
    body{
        background-color: ${(props) => props.theme.baseColor};
        transition: all .5s ease;
    }
`